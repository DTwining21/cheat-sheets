# README #

This repository is mainly for cheatsheets and testing git! enjoy!

### Prerequisites ###

* A bitbucket account
* The latest version of git installed on your computer.
* A program or programs of your choice to open the files in.

### How do I get set up? ###

* Use your Command Prompt to navigate to the folder you want to store the repository in. The "cd" command comes in handy for this. Folder MUST be empty!
* Use command "git init" to create a repository in the current folder.
* Use command "git clone https://DTwining21@bitbucket.org/DTwining21/test.git" to clone this remote repository to your local repository.
* You may have to log in with your bitbucket account. This is a sign that all the previous steps were done correctly!
* If all went as planned, you should see all of this repository's files on your local machine! Good job!

### Adding files ###

* Queue files for commit with the command "git add yourFileName"
* Commit changes with the command "git commit -m 'Your commit message here'"
* Push these changes to the main repository with the command "git push"
